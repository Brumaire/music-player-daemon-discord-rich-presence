# Music Player Daemon / Discord Rich Presence

Author : Lucas Vincent <Lucas.Vincent@entalpi.net>

Feel free to contribute, share and modify.

## Description

A simple script for Music Player Daemon (MPD) users who want to share songs and stream.


If you don't want to share a streaming link, juste replace these strings in your json config :

```
"mpd_http_uri": "",
"mpd_http_port": 80
```

## Install
- Clone the repository or download the files
- Install the needed python packages with pip or your distro package manager
- Adapt the default conf json to your needs
- enjoy

## Dependencies :
Python Packages :
- Xonsh
- MPDClient
- pypresence
- time
- json

## Run
```mpdDiscord2.xsh <json path>```

#!/usr/bin/env xonsh
import time
import json
import sys
from mpd import MPDClient
from pypresence import Presence

# Load config

if len(sys.argv) < 2 :
    print(f"usage : {sys.argv[0]} json-config-file-path")
    sys.exit(1)

try:
        jsonFile   = open(sys.argv[1],"r")
        jsonDict   = json.load(jsonFile)
        jsonFile.close()
	
        mpd_server_address  = jsonDict['mpd_server_address']
        mpd_server_port     = jsonDict['mpd_server_port']
        mpd_http_uri        = jsonDict['mpd_http_uri']
        mpd_http_port       = jsonDict['mpd_http_port']
        mpd_server_password = jsonDict['mpd_server_password']

except:
	print("unable to load config, exiting.")
	sys.exit(1)

if mpd_http_port != 80:
   detailsContent = f"{mpd_http_uri}:{mpd_http_port}"
else:
   detailsContent = mpd_http_uri


class MPD_Current_Song :
      def __init__(self, trackFileName, trackTitle='', trackArtist='', trackAlbum ='') :
        self.trackFileName = trackFileName
        self.trackTitle    = trackTitle
        self.trackArtist   = trackArtist
        self.trackAlbum    = trackAlbum
        self.discordRpcContent = 'null'
        self.computeRPCContent()

      def computeRPCContent(self) :
      	  if (self.trackTitle == '') :
              self.trackFileName = self.trackFileName.split('/')[-1]
              self.discordRpcContent = f"{self.trackFileName}"
	      
          else :
              self.discordRpcContent = f"{self.trackTitle} - {self.trackArtist} ({self.trackAlbum})" 

      def getContent(self) :
     	 return self.discordRpcContent

# Utilitary functions

def isRunning(appName) :
    result = False
    exist = $(pstree | grep @(appName) )
    if(exist != '') :
        result = True
        print(f"{appName} is currently running.")
    return result

def networkAvailable() :
    result = False
    nstate = $(ping -c 1 entalpi.net | grep '64 bytes')
    if (nstate != '') :
       result = True
       print("the network is available.")
    return result

# MPD Client functions

def mpdClientInit() :
    client = MPDClient()
    client.timeout = 10
    client.idletimeout = None
    return client

def isMpdIdleState(client) :
    result = False
    try:
	client.connect(mpd_server_address,mpd_server_port)
    	status = client.status()
    	client.disconnect()
    	if( status['state'] == 'stop') or (status['state'] == 'pause') :
      	    result = True
      	print("The MPD Server is currently idle.")
    except:
	print("Can't talk to the MPD_Server and get the state.")
	result = True
    return result

def mpdTimestamp(connected_client) :
    epoch_time = int(time.time())
    elapsed = int(float(connected_client.status()['elapsed']))
    return epoch_time - elapsed


# Discord RPC Functions

def rpcClientInit() :
    discord_app_id = 738005117294608414
    client = Presence(discord_app_id)
    return client

def getSongMeta(song,key):
    result = f"Unknown {key.title()}"
    if key in song:
       return song[key]
    return result

def updateStreamTitle(mpdClient, rpcClient):
    mpdClient.connect(mpd_server_address,mpd_server_port)
    song      = mpdClient.currentsong()
    timestamp = mpdTimestamp(mpdClient)
    mpdClient.disconnect()

    title  = getSongMeta(song,'title')
    artist = getSongMeta(song,'artist')
    album  = getSongMeta(song,'album')


    if 'title' in song:
        mpdInfo = MPD_Current_Song ( song['file']   \
                                   , title  \
                                   , artist \
                                   , album  \
       	                           )
    else:
        mpdInfo = MPD_Current_Song ( song['file'] ) 

    rpcClient.update (   details     = detailsContent	           \
                     ,   state       = mpdInfo.getContent()        \
                     ,   start       = timestamp                   \
                     ,   large_image = "mpd_streaming"             \
                     )


# Main


mpd_client = mpdClientInit()
rpc_client = rpcClientInit()

while True :
      if isRunning("Discord") and networkAvailable() and not isMpdIdleState(mpd_client):
          i = 12 # periodic update duration : 12*10s = 2min
          rpc_client.connect()
          while not isMpdIdleState(mpd_client) and i > 0 :
             i = i-1
             try: # The connection might fail
                updateStreamTitle(mpd_client,rpc_client)
             except:
                print("Can't poke discord, reconnecting.")
                rpc_client.close()
                rpc_client.connect()
             time.sleep(10)

          # Check if the server is currently idle, and tell discord for the remaining period duration.
	  if isMpdIdleState(mpd_client) :
              while isMpdIdleState(mpd_client) and i > 0 :
                  i = i-1
                  rpc_client.update(state = "server is idle / paused")
                  time.sleep(10)
          rpc_client.clear() # clear the RPC state
          rpc_client.close() # close the client
      else :
      	     print("discord / mpd syncing is currently unavailable because of current state.")
      	     time.sleep(60)
